from django.db import models
import datetime
from django.contrib.auth.models import User

class Corpus(models.Model):
    text_body = models.TextField(default=None)
    text_author = models.CharField(max_length=255)
    text_name = models.CharField(max_length=255)
    word_count = models.IntegerField(default=0)
    genre_text = models.SmallIntegerField(default=0)
    type_text = models.SmallIntegerField(default=0)
    status = models.SmallIntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Tag(models.Model):
    symbol = models.CharField(max_length=255, null=True, blank=True)
    eng_title = models.CharField(max_length=255, null=True, blank=True)
    rus_title = models.CharField(max_length=255, null=True, blank=True)
    kaz_title = models.CharField(max_length=255, null=True, blank=True)
    universal_pos = models.CharField(max_length=255, null=True, blank=True)
    notes = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Sentence(models.Model):
    author = models.TextField(blank=True, null=True)
    source = models.TextField(blank=True, null=True)
    year = models.IntegerField(default=0)
    style = models.CharField(max_length=255)
    sentence = models.TextField(blank=True, null=True)
    eng_sentence = models.TextField(blank=True, null=True)
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Word(models.Model):
    word = models.TextField(blank=True, null=True)
    numeric = models.IntegerField(default=0)
    sentence = models.ForeignKey(Sentence, on_delete = models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Wordform(models.Model):
    wordform = models.TextField(blank=True, null=True)
    word = models.ForeignKey(Word, on_delete = models.CASCADE)
    sentence = models.ForeignKey(Sentence, on_delete = models.CASCADE)
    apertium_output = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Contact(models.Model):
    name = models.TextField(blank=True, null=True)
    email = models.TextField(blank=True, null=True)
    message = models.TextField(blank=True, null=True)
    status = models.IntegerField(default=0)
    answer = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Right(models.Model):
    right = models.CharField(max_length=255, default="guest")
    user = models.OneToOneField(User, on_delete=models.DO_NOTHING)


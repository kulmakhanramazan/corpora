from django import forms

from .models import Tag

class TagForm(forms.ModelForm):

    def __init__(self):
        super(TagForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs = {
            'class': 'form-control col-md-6'
        }
        self.fields['description'].widget.attrs = {
            'class': 'form-control col-md-6'
        }
        self.fields['price'].widget.attrs = {
            'class': 'form-control col-md-6',
            'step': 'any',
            'min': '1',
        }

class Meta:
    model = Tag
    fields = ('eng_title', 'rus_title', 'kaz_title')
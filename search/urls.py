from django.urls import path
from .views import *

urlpatterns = [
    path('', main),
    path('contact', contact),
    path('save_contact', save_contact),
    path('about', about),
    path('search', search),
    path('morph_analysis', morph_analysis),
    path('check', check),   
    path('login', signin),
    path('register', signup),
    ##################################################
    path('expert/dash', expert_dash),
    path('expert/text/<int:id>', show_text),
    path('expert/my_texts', my_texts),
    path('expert/stop-words', stop_words),
    path('expert/tokens', tokens),
    path('expert/translate', translate),
    path('analyze_text', analyze_text),
    ##################################################
    path('admin/dash', admin_dash),
    path('admin/messages', admin_messages),
    path('admin/texts', admin_texts),
    path('admin/users', admin_users),
    path('user_right_update', user_right_update),
    path('admin/tags', admin_tags),
    path('admin/tags/<int:id>/edit', admin_tags_edit),
]



from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
import requests
from streamparser import parse
from .models import Tag, Word, Sentence, Wordform, Contact, Right
import ast
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger, InvalidPage
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
import translators as ts
import json
from django.db.models import Q
from django.utils import timezone
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import apertium


def main(request):
    return render(request, 'pages/main.html')


def about(request):
    return render(request, 'pages/about.html')


@csrf_exempt
def contact(request):
    return render(request, 'pages/contact.html')


@csrf_exempt
def save_contact(request):
    if request.method == "POST":
        if not request.POST.get("name") and request.POST.get("name").strip() == "":
            return JsonResponse({"error": "Заполните поле имя"})
        elif not request.POST.get("email") and request.POST.get("email").strip() == "":
            return JsonResponse({"error": "Заполните поле email"})
        elif not request.POST.get("message") and request.POST.get("message").strip() == "":
            return JsonResponse({"error": "Заполните поле сообщение"})
        else:
            Contact.objects.create(name=request.POST.get("name"),
                                   email=request.POST.get("email"),
                                   message=request.POST.get("message"))
            return JsonResponse({
                "success": "Сообщение которое вы отправили успешно доставлено. Ответ поступить к вам на электронную почту"})
    return JsonResponse({"success": "ОК"})


def search(request):
    if request.method == "POST":
        if request.POST.get('search') or request.POST.get('search').strip() == "":
            json_words = []
            json_wordforms = []

            search_text = ""
            if request.POST.get('search'):
                search_text = request.POST.get('search').strip()

            search_author = ""
            if request.POST.get('author'):
                search_author = request.POST.get('author').strip()

            search_source = ""
            if request.POST.get('source'):
                search_source = request.POST.get('source').strip()

            search_style = ""
            if request.POST.get('style'):
                search_style = request.POST.get('style').strip()

            search_from_year = 0
            if request.POST.get('from_year'):
                search_from_year = request.POST.get('from_year').strip()

            search_to_year = timezone.now().year
            if request.POST.get('to_year'):
                search_to_year = request.POST.get('to_year').strip()

            print("\nSTYlE = ", search_style,
                  "\nTEXT = ", search_text,
                  "\nAUTHOR = ", search_author,
                  "\nSOURCE = ", search_source)

            json_sentences = Sentence.objects.filter(
                Q(sentence__icontains=search_text) | Q(sentence__icontains=search_text.lower()) | Q(
                    sentence__icontains=search_text.capitalize()) | Q(
                    sentence__icontains=search_text.upper())).filter(
                style__contains=search_style).filter(
                Q(author__icontains=search_author) | Q(author__icontains=search_author.lower()) | Q(
                    author__icontains=search_author.capitalize()) | Q(
                    author__icontains=search_author.upper())).filter(
                Q(source__icontains=search_source) | Q(source__icontains=search_source.lower()) | Q(
                    source__icontains=search_source.capitalize()) | Q(
                    source__icontains=search_source.upper())).filter(year__gt=search_from_year,
                                                                     year__lt=search_to_year).order_by('-created_at')

            for json_sentence in json_sentences:
                json_words.append(list(json_sentence.word_set.all().values()))
                json_wordforms.append(list(json_sentence.wordform_set.all().values()))
            json_tags = Tag.objects.all()
            return render(request, 'search/search.html', {"sentences": json_sentences,
                                                          "words": json_words,
                                                          'wordforms': json_wordforms,
                                                          "tags": json_tags,
                                                          "search": search_text,
                                                          "style": search_style,
                                                          "author": search_author,
                                                          "source": search_source,
                                                          "from_year": search_from_year,
                                                          "to_year": search_to_year})
    return render(request, 'search/search.html')


@csrf_exempt
def translate(request):
    if request.method == "POST":
        if request.is_ajax():
            data_json = json.loads(request.body)
            text = data_json['text']
            text = text.strip()
            print("TEXT = ", text)
            if text is None or text == "":
                return JsonResponse({'translated_text': 'Отсутсвует текст для перевода'})
            else:

                translated_text = ts.google(text, to_language='en', from_language='kk')
                return JsonResponse({'translated_text': translated_text})
        else:
            return HttpResponse('Испоользуйте POST запрос c AJAX')
    return HttpResponse('Испоользуйте POST запрос')


@csrf_exempt
@login_required
def morph_analysis(request):
    base_url = "http://127.0.0.1:32768"

    if request.method == "POST":
        errors = ""
        if request.user.right.right != "expert":
            errors += "Надо заходить как expert <br>"
        if not request.POST.get("author") and request.POST.get("author").strip() == "":
            errors += "Надо указать автор текста  <br>"
        if not request.POST.get("source") and request.POST.get("source").strip() == "":
            errors += "Надо указать источник текста <br>"
        if not request.POST.get("style"):
            errors += "Надо выбирать стиль текста <br>"
        if not request.POST.get("sentence") and request.POST.get("sentence").strip() == "":
            errors += "Надо заполнить  текст <br>"
        if request.POST.get("sentence"):
            if len(request.POST.get("sentence")) > 500:
                errors += "Количество символов в тексте не должен превышать более 500"
        if errors != "":
            return JsonResponse({"errors": errors})

        text = request.POST.get('sentence')
        url = f"{base_url}/analyse?lang=kaz&q={text}"
        result = requests.get(url)
        result_list = ast.literal_eval(result.text)

        print(f"\n\n\nResult list: {result_list}\n\n\n")
        # ***********************************************************************************
        sentence = Sentence()
        sentence.sentence = text
        sentence.author = request.POST.get("author")
        sentence.source = request.POST.get("source")
        sentence.year = request.POST.get("year")
        sentence.style = request.POST.get("style")
        sentence.eng_sentence = request.POST.get("eng_sentence")
        sentence.user_id = request.user.id
        sentence.save()
        for result_item in result_list:
            word = sentence.word_set.create(word=result_item[1])
            print('result item: ', word)
            word_forms = parse(f'^{result_item[0]}$')
            for word_form in word_forms:
                for item in word_form.readings:
                    print(f'\nWORD FORM => {item[0][0]},  TAG => {item[0][1]}\n')
                    if len(item) == 1:
                        word.wordform_set.create(wordform=item[0][0], sentence=sentence,
                                                 apertium_output=item[0][1])
        # **********************************************************************************  
        json_words = sentence.word_set.all()
        json_wordforms = []
        for json_word in json_words:
            json_wordforms.append(list(json_word.wordform_set.all().values()))
        json_tags = Tag.objects.all()
        return JsonResponse({"success": sentence.id})
    return JsonResponse({"success": "OK"})


@csrf_exempt
@login_required
def analyze_text(request):
    if request.method == "POST":
        errors = ""
        if request.user.right.right != "expert":
            errors += "Надо заходить как expert <br>"
        if not request.POST.get("author") and request.POST.get("author").strip() == "":
            errors += "Надо указать автор текста  <br>"
        if not request.POST.get("source") and request.POST.get("source").strip() == "":
            errors += "Надо указать источник текста <br>"
        if not request.POST.get("style"):
            errors += "Надо выбирать стиль текста <br>"
        if not request.POST.get("sentence") and request.POST.get("sentence").strip() == "":
            errors += "Надо заполнить  текст <br>"
        if request.POST.get("sentence"):
            if len(request.POST.get("sentence")) > 500:
                errors += "Количество символов в тексте не должен превышать более 500"
        if errors != "":
            return JsonResponse({"errors": errors})

        text = request.POST.get('sentence')

        sentence = Sentence()
        sentence.sentence = text
        sentence.author = request.POST.get("author")
        sentence.source = request.POST.get("source")
        sentence.year = request.POST.get("year")
        sentence.style = request.POST.get("style")
        sentence.eng_sentence = request.POST.get("eng_sentence")
        sentence.user_id = request.user.id
        sentence.save()
        try:
            analyzed_result = apertium.analyze('kaz', text)
        except apertium.ModeNotInstalled:
            apertium.install_module('kaz')
            analyzed_result = apertium.analyze('kaz', text)

        for analyzed_item in analyzed_result:
            parsed_result = list(parse(f'^{analyzed_item}$'))
            for parsed_item in parsed_result:
                word = sentence.word_set.create(word=parsed_item.wordform)
                for readings in parsed_item.readings:
                    for reading in readings:
                        if len(reading.tags) == 0:
                            unknown_word_analyze = list(apertium.analyze('kk', parsed_item.wordform.lower()))
                            unknown_word_parsed = list(parse(f'^{unknown_word_analyze}$'))
                            for unknown_word_parsed_item in unknown_word_parsed:
                                for unknown_readings in unknown_word_parsed_item.readings:
                                    for unknown_reading in unknown_readings:
                                        word.wordform_set.create(wordform=unknown_reading.baseform,
                                                                 sentence=sentence,
                                                                 apertium_output=unknown_reading.tags)
                        else:
                            word.wordform_set.create(wordform=reading.baseform,
                                                     sentence=sentence,
                                                     apertium_output=reading.tags)
        json_words = sentence.word_set.all()
        json_wordforms = []
        for json_word in json_words:
            json_wordforms.append(list(json_word.wordform_set.all().values()))
        json_tags = Tag.objects.all()
        return JsonResponse({"success": sentence.id})
    return JsonResponse({"success": "OK"})


def show_text(request, id):
    json_words = []
    json_wordforms = []
    json_sentences = Sentence.objects.filter(id=id)
    for json_sentence in json_sentences:
        json_words.append(list(json_sentence.word_set.all().values()))
        json_wordforms.append(list(json_sentence.wordform_set.all().values()))
    json_tags = Tag.objects.all()
    print(json_sentences)
    print("JSON wordforms: ", json_wordforms)
    return render(request, 'expert/show.html', {"sentences": json_sentences,
                                                "words": json_words,
                                                'wordforms': json_wordforms,
                                                "tags": json_tags,
                                                "search": search})


def my_texts(request):
    json_words = []
    json_wordforms = []
    json_sentences = Sentence.objects.filter(user_id=request.user.id).order_by('-created_at')
    paginator = Paginator(json_sentences, 1)
    page = request.GET.get("page")
    try:
        json_sentences = paginator.get_page(page)
    except PageNotAnInteger:
        print("Is not an integer")
    except EmptyPage:
        print("Is a EmptyPage")
    except InvalidPage:
        print("That is invalid page")
    for json_sentence in json_sentences:
        json_words.append(list(json_sentence.word_set.all().values()))
        json_wordforms.append(list(json_sentence.wordform_set.all().values()))
    json_tags = Tag.objects.all()
    print(json_sentences)
    print("Wordforms:", json_wordforms)
    return render(request, 'expert/my_texts.html', {"sentences": json_sentences,
                                                    "words": json_words,
                                                    'wordforms': json_wordforms,
                                                    "tags": json_tags,
                                                    "search": search,
                                                    'for_pagination': json_sentences.paginator.num_pages - 2
                                                    })


def signup(request):
    if request.method == "POST":
        errors = ""
        if not request.POST.get("first_name") or request.POST.get("first_name").strip() == "":
            errors += "Необходимо заполнить поле имя<br>"
        if not request.POST.get("last_name") or request.POST.get("last_name").strip() == "":
            errors += "Необходимо заполнить поле фамилия<br>"
        if not request.POST.get("email") or request.POST.get("email").strip() == "":
            errors += "Необходимо заполнить поле email<br>"
        if not request.POST.get("username") or request.POST.get("username").strip() == "":
            errors += "Необходимо заполнить поле логин<br>"
        if not request.POST.get("password") or request.POST.get("password").strip() == "":
            errors += "Необходимо заполнить поле пароль<br>"
        if request.POST.get("username"):
            any_user = User.objects.filter(username__contains=request.POST.get("username")).count()
            print(any_user)
            if any_user > 0:
                errors += "Такой логин уже существует в базе! Пожалуйста, придумайте другой логин."

        if errors != "":
            return JsonResponse({"errors": errors})
        else:
            user = User.objects.create_user(first_name=request.POST.get("first_name"),
                                            last_name=request.POST.get("last_name"),
                                            email=request.POST.get("email"),
                                            username=request.POST.get("username"),
                                            password=request.POST.get("password"))
            user.save()
            right = Right.objects.create(user_id=user.id)
            return JsonResponse({"success": "Вы успешно зарегистрованы!"})

    return render(request, "pages/signup.html")


def signin(request):
    return redirect("/accounts/login")


@login_required
def check(request):
    if request.user.right:
        if request.user.is_superuser == 1:
            return redirect('/admin/dash')
        elif request.user.right.right == "expert":
            return redirect('/expert/dash')
        else:
            return redirect("/")
    else:
        return redirect('/')


def expert_dash(request):
    return render(request, "expert/dash.html")


def admin_dash(request):
    users = User.objects.all().count()
    messages = Contact.objects.all().count()
    texts = Sentence.objects.all().count()
    return render(request, 'admin/dash.html', {
        "users": users,
        "messages": messages,
        "texts": texts
    })


def admin_messages(request):
    messages = {}
    messages_list = Contact.objects.get_queryset().order_by('-created_at')
    paginator = Paginator(messages_list, 10)
    page = request.GET.get("page")
    try:
        messages = paginator.get_page(page)
    except PageNotAnInteger:
        print("Is not an integer")
    except EmptyPage:
        print("Is a EmptyPage")
    except InvalidPage:
        print("That is invalid page")
    return render(request, 'admin/messages.html',
                  {'messages': messages, 'for_pagination': messages.paginator.num_pages - 2})


def admin_users(request):
    users = {}
    users_list = User.objects.get_queryset().order_by('-last_login')
    paginator = Paginator(users_list, 10)
    page = request.GET.get("page")
    try:
        users = paginator.get_page(page)
    except PageNotAnInteger:
        print("Is not an integer")
    except EmptyPage:
        print("Is a EmptyPage")
    except InvalidPage:
        print("That is invalid page")
    return render(request, 'admin/users.html', {'users': users, 'for_pagination': users.paginator.num_pages - 2})


def admin_tags(request):
    tags_list = Tag.objects.get_queryset().order_by('symbol')
    paginator = Paginator(tags_list, 10)
    page = request.GET.get("page")
    try:
        tags = paginator.get_page(page)
    except PageNotAnInteger:
        print("Is not an integer")
    except EmptyPage:
        print("Is a EmptyPage")
    except InvalidPage:
        print("That is invalid page")
    return render(request, 'admin/tags.html', {'tags': tags, 'for_pagination': tags.paginator.num_pages - 2})


def admin_tags_edit(request, id, template_name='admin/edit.html'):
    if request.method == "POST":
        Tag.objects.filter(id=request.POST.get("id")).update(
            eng_title=request.POST.get("eng_title").strip(),
            rus_title=request.POST.get("rus_title").strip(),
            kaz_title=request.POST.get("kaz_title").strip(),
        )
    tag = Tag.objects.get(id=id)
    return render(request, 'admin/tags_edit.html', {'tag': tag})


def admin_texts(request):
    json_words = []
    json_wordforms = []
    json_sentences = Sentence.objects.all().order_by('-created_at')
    paginator = Paginator(json_sentences, 10)
    page = request.GET.get("page")
    try:
        json_sentences = paginator.get_page(page)
    except PageNotAnInteger:
        print("Is not an integer")
    except EmptyPage:
        print("Is a EmptyPage")
    except InvalidPage:
        print("That is invalid page")
    for json_sentence in json_sentences:
        json_words.append(list(json_sentence.word_set.all().values()))
        json_wordforms.append(list(json_sentence.wordform_set.all().values()))
    json_tags = Tag.objects.all()
    print(json_sentences)
    return render(request, 'admin/texts.html', {"sentences": json_sentences,
                                                "words": json_words,
                                                'wordforms': json_wordforms,
                                                "tags": json_tags,
                                                "search": search,
                                                'for_pagination': json_sentences.paginator.num_pages - 2
                                                })


@csrf_exempt
def user_right_update(request):
    if request.method == "POST":
        right = Right.objects.get(user_id=request.POST.get("id"))
        right.right = request.POST.get("right")
        right.save()
        print(request.POST.get("right"))
        return JsonResponse({"success": "OK"})
    return JsonResponse({"success": "OK"})


@csrf_exempt
def stop_words(request):
    text = ""
    filtered_text = []
    stop_words = set()
    lang = ""
    if request.method == "POST":
        if request.POST.get('text'):
            text = request.POST.get('text').strip()

        if request.POST.get('lang'):
            lang = request.POST.get('lang')
        print("LANG = ", lang)

        stop_words = set(stopwords.words(lang))
        print("STOPWORS", stop_words)
        word_tokens = word_tokenize(text)

        filtered_text = [w for w in word_tokens if not w in stop_words]
        filtered_text = [w for w in word_tokens if not w.lower() in stop_words]

        print("FILTERED_TEXT = ", filtered_text)
        print("TOKENS = ", word_tokens)

        # for w in word_tokens:
        #     if w not in stop_words:
        #         filtered_text.append(w)
        print(word_tokens)
        print(filtered_text)

        return render(request, 'expert/stop-words.html',
                      {'filtered_text': filtered_text, 'text': text, 'lang': lang, 'stop_words': stop_words})
    else:
        return render(request, 'expert/stop-words.html',
                      {'filtered_text': filtered_text, 'text': text, 'lang': lang, 'stop_words': stop_words})


@csrf_exempt
def tokens(request):
    text = ""
    if request.POST.get('text'):
        text = request.POST.get('text').strip()

    lang = ""
    if request.POST.get('lang'):
        lang = request.POST.get('lang')
    print("LANG = ", lang)

    tokens = word_tokenize(text)
    print("TOKENS = ", tokens)

    return render(request, 'expert/tokens.html',
                  {'tokens': tokens, 'text': text, 'lang': lang})

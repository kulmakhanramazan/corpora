from django import template

register = template.Library()


@register.filter
def get_string_as_list(value):
    if '[' and ']' in value:
        return eval(value)

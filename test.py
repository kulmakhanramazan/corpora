# -*- coding: utf-8 -*-

import apertium
from streamparser import parse

sentence = 'Қазіргі заманда түрлі лингвистикалық зерттеулер жүргізу кезінде компьютерлік лингвистика барған сайын кеңінен қолданылады . Қолданбалы компьютерлік лингвистикасының маңызды әдіснамалық проблемасы автоматты мәтінді өңдеу жүйелерінің декларативті және рәсімдік компоненттері арасындағы қажетті қарым-қатынасты дұрыс бағалау болып табылады'

try:
    result = list(apertium.analyze('kk', sentence))
except apertium.ModeNotInstalled:
    apertium.install_module('kk')
    result = list(apertium.analyze('kk', sentence))



print('\n\nSentense: ', sentence, '\n\n')

print('Result: ', result, '\n\n')
for i in result:
    parsed_list = list(parse(f'^{i}$'))
    print(parsed_list)
    for parsed_item in parsed_list:
        print('Wordform: ', parsed_item.wordform)
        print('Readings: ', parsed_item.readings)
        for reading in parsed_item.readings:
            for item in reading:
                print('Reading: ', item)
                print('Item baseform: ', item.baseform, '=>', item.tags)
                if len(item.tags) == 0:
                    print("Нет: ", item.tags)
                    non_tags = list(apertium.analyze('kk', parsed_item.wordform.lower()))
                    non_tags_list = list(parse(f'^{non_tags}$'))
                    print('Parsed list: ', non_tags_list)
                    for non_tags_item in non_tags_list:
                        for n_reading in non_tags_item.readings:
                            for n_item in n_reading:
                                print('Non tag item: ', n_item.baseform, '=>', n_item.tags)

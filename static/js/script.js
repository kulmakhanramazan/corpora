$(document).ready(function (e) {
    $(function () {
        $("[data-toggle=popover]").popover({
            html: true,
            sanitize: false,
            container: 'body',
            placement: 'top',
        })
    });


    var pathname = window.location.pathname;
    let a_tag = $('a[href="' + pathname + '"]');
    a_tag.addClass("active");
    let dropdown_a_tag = $('.dropdown-menu a[href="' + pathname + '"]');
    if (dropdown_a_tag.length > 0) $("#main-menu").addClass("active");

    $(".results span").each(function (index) {
        var src_str = $(this).text();
        var term = $("input[name=search]").val();
        term = term.replace(/(\s+)/, "(<[^>]+>)*$1(<[^>]+>)*");
        var pattern = new RegExp("(" + term + ")", "gi");

        src_str = src_str.replace(pattern, "<mark>$1</mark>");
        src_str = src_str.replace(/(<mark>[^<>]*)((<[^>]+>)+)([^<>]*<\/mark>)/, "$1</mark>$2<mark>$4");

        $(this).html(src_str);
    });

    $("#analysis-form").on("submit", function (event) {
        event.preventDefault();
        var text = $("#analysis-editor").val();
        $("#analysis-button").css("display", "none");
        $("#analysis-loading").css("display", "block");

        $.ajax({
            type: "POST",
            url: "/morph_analysis",
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (data) {
                $("#analysis-result").val(data.result);
                $("#analysis-button").css("display", "block");
                $("#analysis-loading").css("display", "none");
            }
        });


    });
    // $("#analysis-form").on("submit",function( event){
    //     event.preventDefault();
    //    var text = $("#analysis-editor").val();
    //    $("#analysis-button").css("display", "none");
    //    $("#analysis-loading").css("display", "block");

    //    $.ajax({
    //       type: "POST",
    //       url: "http://192.168.99.100:32768/analyse?lang=kaz&q="+text,
    //       data: new FormData(this),
    //        processData: false,
    //        contentType: false,
    //       success: function(data){
    //            var arr = data;
    //            arr.forEach(element => {
    //                console.log(element[0])
    //                console.log(element[1])

    //            });
    //             $("#analysis-result").val(data);
    //             $("#analysis-button").css("display", "block");
    //             $("#analysis-loading").css("display", "none");
    //       }
    //    });


    // });

    var formsubmit = $("#formsubmit");
    formsubmit.click(function () {
        $("#msgSubmit").html("");
        $("#msgSubmit").css("display", "none");
        $("#okSubmit").html("");
        $("#okSubmit").css("display", "none");
        var emailsubmit = $("#emailsubmit");
        var namesubmit = $("#namesubmit");
        var messagesubmit = $("#messagesubmit");
        if (namesubmit.val().trim() == "") {
            $("#msgSubmit").html("Пожалуйста, укажите имя");
            $("#msgSubmit").css("display", "block");
        } else if (emailsubmit.val().trim() == "") {
            $("#msgSubmit").html("Пожалуйста, укажите email");
            $("#msgSubmit").css("display", "block");
        } else if (messagesubmit.val().trim() == "") {
            $("#msgSubmit").html("Пожалуйста, заполните поле сообщение");
            $("#msgSubmit").css("display", "block");
        } else {
            $.ajax({
                url: "/save_contact",
                type: "POST",
                data: {
                    name: namesubmit.val(),
                    email: emailsubmit.val(),
                    message: messagesubmit.val()
                },
                success: function (data) {
                    if (data.success) {
                        $("#okSubmit").html(data.success);
                        $("#okSubmit").css("display", "block");
                    } else {
                        $("#msgSubmit").html(data.error);
                        $("#msgSubmit").css("display", "block");
                    }
                },
                error: function (data) {
                    $("#msgSubmit").html(data);
                    $("#msgSubmit").css("display", "block");
                }
            })
        }
    });

    $("#signup-form").submit(function (e) {
        e.preventDefault();
        $("#signup-error").html("");
        $("#signup-error").css("display", "none");
        $("#signup-success").html("");
        $("#signup-success").css("display", "none");
        $.ajax({
            type: "POST",
            url: "/signup",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                if (data.errors) {
                    $("#signup-error").html(data.errors);
                    $("#signup-error").css("display", "block");
                } else if (data.success) {
                    $("#signup-success").html(data.success);
                    $("#signup-success").css("display", "block");
                } else {
                    $("#signup-error").html(data);
                    $("#signup-error").css("display", "block");
                }
            },
            error: function (data) {
                $("#signup-error").html(data.errors);
                $("#signup-error").css("display", "block");
            }
        });
    });


});


var getSelected = function () {
    var t = '';
    if (window.getSelection) {
        t = window.getSelection();
    } else if (document.getSelection) {
        t = document.getSelection();
    } else if (document.selection) {
        t = document.selection.createRange().text;
    }
    return t;
}

$("#analysis-editor").select(function (eventObject) {
    $("#selected-text").html(getSelected().toString());
});


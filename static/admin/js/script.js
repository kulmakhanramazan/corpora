$("#analysis-form").submit(function (event) {
    event.preventDefault();
    var submitButton = $("#analysis-submit-button");
    var errorBlock = $("#analysis-error");
    submitButton.toggleClass("running");
    submitButton.toggleClass("disabled");
    errorBlock.css("display", "none")
    $.ajax({
        type: "POST",
        url: "/analyze_text",
        data: $(this).serialize(),
        success: function (data) {
            if (data.errors) {
                errorBlock.html(data.errors);
                errorBlock.css("display", "block");
                submitButton.toggleClass("running");
                submitButton.toggleClass("disabled");
            } else if (data.success) {
                window.location.href = "/expert/text/" + data.success
            } else {
                errorBlock.html(data);
                errorBlock.css("display", "block");
                submitButton.toggleClass("running");
                submitButton.toggleClass("disabled");
            }
        },
        error: function (data) {
            errorBlock.html(data);
            errorBlock.css("display", "block");
            submitButton.toggleClass("running");
            submitButton.toggleClass("disabled");
        }
    })
});

$("#translate_button").click(function () {
    var translateButton = $("#translate_button");
    translateButton.toggleClass("disabled");
    translateButton.toggleClass("running");
    $.ajax({
        type: "POST",
        url: "/expert/translate",
        contentType: 'application/json',
        data: JSON.stringify({
            "text": $("#sentence_kk").val(),
        }),
        success: function (data) {
            console.log(data.translated_text)
            $("#sentence_en").val(data.translated_text)
        },
        error: function (data) {
            alert("Ошибка!");
        },
        complete: function () {
            translateButton.toggleClass("disabled");
            translateButton.toggleClass("running");
        }
    })
})


$(function () {
    $("[data-toggle=popover]").popover({
        html: true,
        // sanitize: false,
        container: 'body',
        placement: 'top',
    })


    $('.multiselect').fSelect();

})

$(document).ready(function () {
    $("select.user-right").change(function () {
        var right = $(this).children("option:selected").val();
        var form = $(this).parent("form");
        var id = form.children("input:hidden").val();
        $.ajax({
            type: "POST",
            url: "/user_right_update",
            data: {
                id: id,
                right: right
            },
            success: location.reload()
        })
    });
})